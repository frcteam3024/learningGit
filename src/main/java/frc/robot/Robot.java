package frc.robot;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj2.command.CommandScheduler;

public class Robot extends TimedRobot {

  @Override
  public void robotInit() {
    /** This function is called once each time the robot is initialized */
  } 

  @Override
  public void robotPeriodic() {
    /** This function called periodically while the robot is running */
    CommandScheduler.getInstance().run();
  }

}
