package frc.robot;

import frc.robot.Constants.OIConstants;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.command.Command;
import frc.robot.commands.TankDriveCmd;
import frc.robot.subsystems.DriveSubsystem;

public class RobotContainer {
  
  private final DriveSubsystem driveSubsystem = new DriveSubsystem();
  private final Joystick copilotController = new Joystick(OIConstants.COPILOT_JOYSTICK_PORT);

  public RobotContainer() {

    driveSubsystem.setDefaultCommand(
      new TankDriveCmd(
        driveSubsystem,
        () -> getCopilotRawAxis(OIConstants.COPILOT_LEFT_Y_AXIS),
        () -> getCopilotRawAxis(OIConstants.COPILOT_RIGHT_Y_AXIS)
      )
    );

    configureButtonBindings();

  }
  
  public double getCopilotRawAxis(int axis) {
    return copilotController.getRawAxis(axis);
  }

  private Command configureButtonBindings() {
    return null;
  }
}