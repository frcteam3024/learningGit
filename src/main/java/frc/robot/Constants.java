package frc.robot;

public final class Constants {
  private Constants() {}

  public static final class DriveConstants {
    private DriveConstants() {}
    
    public static final int MOTOR_LEFT_1_ID  = 0;
    public static final int MOTOR_LEFT_2_ID  = 0;
    public static final int MOTOR_RIGHT_1_ID = 0;
    public static final int MOTOR_RIGHT_2_ID = 0;
  }

  public static final class OIConstants {
    private OIConstants() {}

    public static final int COPILOT_JOYSTICK_PORT = 0;
    public static final int COPILOT_LEFT_Y_AXIS   = 0;
    public static final int COPILOT_RIGHT_Y_AXIS  = 0;
  }

}
