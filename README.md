# Before Starting

0 Watch [this video](https://youtu.be/watch?v=USjZcfj8yxE) to learn some of the important fundamental git concepts

# Setup

1 Make [gitlab](https://gitlab.com/users/sign_up) account

2 Email me or message me on discord with your username so I can invite you to the project

3 Install [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

4 Install [vscode](https://code.visualstudio.com/download) on your computer, then install the wpilib vscode extension

# Getting Started

5 Open up your git environment (terminal on linux/mac, git bash on windows)

6 Make sure git is properly installed by running `git --version`. If you get an error, go back and fix your installation

7 Run the following commands to set up your git environment: 
```bash
git config --global user.name "your name"
git config --global user.email "your email"
```

8 Create the project on your computer by running `git clone https://gitlab.com/frcteam3024/learningGit`

9 Move into the directory you just created with the command `cd learningGit` (`cd` means "change into directory ...")

10 There are several different "branches" of our robot code, several for our robot from last year or for implementing various features. These are important, but what we are interested in is the branch for the parade robot, containing the completed code of "skeleton" you filled out earlier. Access that branch by running `git checkout parade`

11 Run `git status`. Because you haven't made any changes yet, it should say that the working directory is clean

12 Open up the folder containing your git project in vscode

13 Navigate to the Robot.java file using the menu on the left-hand side. It should be under the path `src/main/java/frc/robot`

# Bug Fix

14 When I wrote the robot code for the skeleton, it was never tested on physical hardware, so inevitably there was a bug. Fixing this will allow an opportunity to use the theoretical knowledge from the video for a real application. To fix this, insert the following line to create a local variable: 
```java
public static RobotContainer robotContainer;
```
This should be placed as the very first line inside of the `Robot` class. Then, inside the `robotInit` method, add the line 
```java
robotContainer = new RobotContainer();
```
Essentially, this initializes the RobotContainer class which handles joystick inputs, a necessary function that the code cannot function without.

15 Save the file, then run `git status` in the terminal. You should now see that Robot.java has been modified.

16 Use `git add src/main/java/frc/robot/Robot.java`. Run `git status` once more and you should see that the file is now ready to be committed

17 Run `git commit -m "some useful message"`, but actually please give it a useful message, maybe something like "robotContainer initialization bug fix". You can get away with nonsense messages on a small tutorial project like this, but it would make your life a nightmare if you were developing code for a larger project, like our robot, for example. One way to think about a commit is like a checkpoint: once you reach it, your progress is saved. Then at any point in the future you can decide to load the state of the project at that checkpoint.

# New Feature

18 Suppose now we want to add a new feature to the code. This should be done in a separate branch so nothing accidentally messes up the current functioning code of the main branch. (Trust me, I have made this mistake several times) To create a new branch, run `git branch new-feature-your-name`. Your name is needed so that when you push your changes, I can go through and see if you did the work and if you did it correctly. Be sure that your changes are on your new feature branch, not the main branch! (You can check the branch you are currently on with `git branch`, and the one with the asterisk is your current branch)

19 Issue: When only a small amount of power is outputted to the motors of the robot, it might not be enough to overcome friction, and then the robot doesn't move. If the motors stay in this state long enough, they can eventually burn up, which is obviously not ideal. (Don't quote me on that though, Davis is the one to ask about how motors work.)

Solution: If the amount of power being sent to the motors is less than 5% forward OR backward, instead don't send any power. Code should go after calculating `leftMotorOutputs` and `rightMotorOutputs`, but before calling `setLeftMotorOutputs` and `setRightMotorOutputs`. Once you have written code to do this, commit the changes, then run `git push origin new-feature-your-name` to finalize them. And remember, `git status` is your friend!
